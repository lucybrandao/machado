# welcome to MACHADO project

para gerar o arquivo com as dependências:

```bash
pip-compile --generate-hashes requirements.in && pip-compile --generate-hashes requirements-dev.in
```

para instalar as dependências:

```bash
pip-sync requirements-dev.txt
```